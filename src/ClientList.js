import React, { Component } from 'react';
import { Button, ButtonGroup, Container, Table } from 'reactstrap';
import AppNavbar from './AppNavbar';
import { Link } from 'react-router-dom';

class ClientList extends Component {

    constructor(props) {
        super(props);
        this.state = {meals: []};
        this.remove = this.remove.bind(this);
    }

    componentDidMount() {
        fetch('/meals')
                .then(response => response.json())
                .then(data => this.setState({meals: data}));
    }

    async remove(id) {
        await fetch(`/meals/${id}`, {
            method: 'DELETE',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        }).then(() => {
            let updatedMeals = [...this.state.meals].filter(i => i.id !== id);
            this.setState({meals: updatedMeals});
        });
    }

    render() {
        const {meals} = this.state;

        const mealList = meals.map(meal => {
            return <tr key={meal.id}>
                <td style={{whiteSpace: 'nowrap'}}>{meal.name}</td>
                <td>{meal.origin}</td>
                <td>
                    <ButtonGroup>
                        <Button size="sm" color="primary" tag={Link} to={"/meals/" + meal.id}>Edit</Button>
                        <Button size="sm" color="danger" onClick={() => this.remove(meal.id)}>Delete</Button>
                    </ButtonGroup>
                </td>
            </tr>
        });

        return (
                <div>
                    <AppNavbar/>
                    <Container fluid>
                        <div className="float-right">
                            <Button color="success" tag={Link} to="/meals/new">Add meal</Button>
                        </div>
                        <h3>meals</h3>
                        <Table className="mt-4">
                            <thead>
                            <tr>
                                <th width="30%">Name</th>
                                <th width="30%">Origin</th>
                                <th width="40%">Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            {mealList}
                            </tbody>
                        </Table>
                    </Container>
                </div>
        );
    }
}

export default ClientList;
